client: bicola.o client.o
	g++ -o client bicola.o client.o

bicola.o: bicola.h bicola.cpp
	g++ -c bicola.cpp

client.o: bicola.h client.cpp
	g++ -c client.cpp

clean:
	rm *.o .bicola.h.swp .bicola.cpp.swp .client.cpp.swp
