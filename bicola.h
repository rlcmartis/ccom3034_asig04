/***************************************************************************
 *                                                                         *
 *  Author: Ramon Luis Collazo Martis                                      *
 *  Submit Date: October 15, 2013                                          *
 *  e-mail: rlcmartis@gmail.com                                            *
 *                                                                         *
 *  This header file defines the data types "Node" and "bicola".           *
 *  Above every member and member function there is a description of what  *
 *  the member will contain or do.                                         *
 *                                                                         *
 ***************************************************************************/

#ifndef BICOLA_H
#define BICOLA_H

typedef int ElementType;

class Node{
   public:
      /** It will contain the data inserted by the user **/
      ElementType data;
      /** It will point to the next Node **/ 
      Node *next;
      /** It will point to the previous Node **/
      Node *prev;
   /** We have to declare "bicola" a friend so it can use Node's members **/
   friend class bicola;
};

class bicola{
   private:
      /** Since bicola means double ended que, we need two pointers 
          for the first and the last Node **/
      Node *front;  //This will point to the first Node
      Node *back;   //This will point to the last Node
      
   public:
      /** CONSTRUCTOR makes front and back pointers to point NULL **/
      bicola();
      /** Returns true if the bicola is empty **/
      bool empty();
      /** Inserts an element before the first Node **/
      void push_front(ElementType e);
      /** Inserts an element after the last Node **/
      void push_back (ElementType e);
      /** Removes the first Node **/
      void pop_front();
      /** Removes the last Node **/
      void pop_back();
      /** Returns the data of the first Node **/
      ElementType get_front();
      /** Returns the data of the last Node **/
      ElementType get_back();
      /** Shows the content in the bicola **/
      void print();
      /** DESTRUCTOR deletes all the dynamic memory created in a bicola **/
      ~bicola();
};

#endif
