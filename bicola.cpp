/***************************************************************************
 *                                                                         *
 *  Author: Ramon Luis Collazo Martis                                      *
 *  Submit Date: October 15, 2013                                          *
 *  e-mail: rlcmartis@gmail.com                                            *
 *                                                                         *
 *  This class file implements the member functions of the class "bicola". *
 *                                                                         *
 ***************************************************************************/

#include "bicola.h"
#include <iostream>
#include <cstdlib>
using namespace std;

/** CONSTRUCTOR makes front and back pointers to point NULL **/
bicola::bicola(){
   /* We have to define a new bicola as empty and by definition it requires
      to make pointers front and back point to NULL */
   front = back = NULL;
}

/** Returns true if the bicola is empty **/
bool bicola::empty(){
   /* It's not necessary to verify if back is pointing NULL, if the front
      pointer is pointing NULL is because there is no Nodes int the bicola
      and hence it's empty */
   if (front == NULL) return true;
   return false;
}

/** Inserts an element before the first Node **/
void bicola::push_front(ElementType e){
   /* We create the new Node/element with the given data */
   Node *p = new Node;
   p->data = e;
   p->prev = NULL; //Because it will be the first one they will be no prev Node

   /* For the case of an empty bicola we just need to make pointers front and
      back to point the new and unique Node */
   if (empty()){
      front = back = p;
      p->next = NULL;
      return;
   }

   /* If the bicola is not empty we have to make p point to the node which
      front is pointing, make the first Node prev pointer to point the new
      node and then make front point to the new first node. */
   p->next = front;
   front->prev = p;
   front = p;

   return;
}

/** Inserts an element after the last Node **/
void bicola::push_back (ElementType e){
   /* We create the new Node/element with the given data */
   Node *p = new Node;
   p->data = e;
   p->next = NULL; //Because it will be the last one they will be no next Node

   /* For the case of an empty bicola we just need to make pointers front and
      back to point the new and unique Node */
   if (empty()){
      front = back = p;
      p->prev = NULL;
      return;
   }

   /* If the bicola is not empty we have to make the last Node to point
      the new last node, make its previous pointer point the ex-last Node
      and back to point the new one. */
   back->next = p;
   p->prev = back;
   back = p;

   return;
}

/** Removes the first Node **/
void bicola::pop_front(){
   /* If there is nothing to delete, just return */
   if(empty()) return;
   
   /* If there is just one node, we delete it and make front and back to 
      point NULL */
   if(front == back){
      delete back;
      front = back = NULL;
      return;
   }

   /* We create a pointer to point the first node, make front to point the 
      second node, the second node now points NULL with previous because is 
      the new first Node, and then we delete the Node which the new pointer 
      is pointing */
   Node *p = front;
   front = front->next;
   front->prev = NULL;
   delete p;
   return;
}

/** Removes the last Node **/
void bicola::pop_back(){
   /* If there is nothing to delete, just return */
   if(empty()) return;

   /* If there is just one node, we delete it and make front and back to
      point NULL */
   if(front == back){
      delete front;
      front = back = NULL;
      return;
   }

   /* We create a pointer to point the penultimate Node, delete the last one,
      make the new final node to point NULL and back to point that node */
   Node *p = back->prev;
   delete back;
   p->next = NULL;
   back = p;
   return;
}

/** Returns the data of the first Node **/
ElementType bicola::get_front(){
   /* If this function is used and the bicola is empty it will produce an
      error, so we better make it abruptly exit from the program with a
      cout error specifying why */
   if (empty()){
      cerr << "There is no elements in the deque for get_front function.\n";
      exit(1);
   }

   /* We just have to return the data of the first node */
   return front->data;
}

/** Returns the data of the last Node **/
ElementType bicola::get_back(){
   /* If this function is used and the bicola is empty it will produce an
      error, so we better make it abruptly exit from the program with a
      cout error specifying why */
   if (empty()){
      cerr << "There is no elements in the deque for get_front function.\n";
      exit(1);
   }

   /* We just have to return the data of the last node */
   return back->data;
}

/** Shows the content in the bicola **/
void bicola::print(){
   /* If the bicola is empty just say it */
   if (empty()){
      cout << "Empty deque." << endl;
      return;
   }

   /* We create a pointer to go througout the nodes and printing their
      datas */
   Node *p = front;
   cout << "[";
   while (p->next != NULL){
      cout << p->data << ", ";
      p = p->next;
   }
   cout << p->data << "]";
   return;
}

/** DESTRUCTOR deletes all the dynamic memory created in a bicola **/
bicola::~bicola(){
   /* We summon the function pop_back while the bicola have elements */
   while(!empty()) pop_back();
}
