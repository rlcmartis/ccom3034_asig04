#include<iostream>
#include<cassert>
#include"bicola.h"
using namespace std;

void test();

int main(){
   test();
   cout << ":)" << endl;

   return 0;
}

void test(){
   bicola A, B;
   assert(A.empty() && B.empty());
   for (int i=0; i < 9 ; i++){
      A.push_back(i+1);
      B.push_front(i+1);
   }
   for (int i=0; i < 9 ; i++){
      assert(A.get_front() == B.get_back());
      A.pop_front();
      B.pop_back();
   }
   assert(A.empty() && B.empty());
}
